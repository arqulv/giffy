# giffy
HTTP api service for giffy
### Instalation

install external dependencies:
libmagic,  
postgres,  
memcached  

### Schema defentions
```/conf/schemas.sql```

```conf/local.py``` - contains db name and auth settings

```
virtualenv env
source env/bin/activate
pip install -r conf/requirements.txt
```

### Run server
```./giffy.py ```  
```./giffy.py --config=local```  
```./giffy.py --config=prod```  

### Endpoints
```'/users/login' (POST)``` - login  

```'/gifs/:id' (GET)``` - gif info  
```'/gifs' (GET)``` - gifs list  
```'/gifs' (POST)``` - gif upload  

```'/likes/:gif_id' (GET)``` - gif likes info  
```'/likes' (POST)``` - create like  
```'/likes/:id' (DELETE)``` - delete like  

```'/tags/:id' (GET)``` - gif tags info  

```'/following/:id' (GET)``` - user following info  
```'/followers/:id' (GET)``` - user followers info  

```'/follow' (POST)``` - follow user  
```'/follow/:id' (GET)``` - unfollow user  

```'/users' (POST)``` - registration  
```'/users' (GET)``` - users list  
```'/users/:id' (GET)``` - user info  

```'/feed' (GET)``` - giffy feed  
```'/feed/user/:id' (GET)``` - user feed list  
```'/feed/following' (GET)``` - user feed list  
```'/feed/tag/:tag' (GET)``` - feed by tag  
