import logging

import os
from tornado import gen

import giffy
from giffy.endpoints import AuthRequestHandler
from giffy import sugar as s
import magic
from psycopg2 import IntegrityError

class LikeHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, gif_id=None):
        if gif_id == None:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS)

        gif_id = s.str2int(gif_id)
        gif = yield self.gif.get(gif_id)
        if gif == None:
            raise giffy.Error(giffy.Error.NOT_FOUND, "Gif not found")

        likes = yield self.like.get(gif_id)
        self.write_json(likes)

    def post(self):
        gif_id = self.get_json_argument("gif_id", None)
        account_id = self.account_id

        gif = yield self.gif.get(gif_id)
        if gif == None:
            raise giffy.Error(giffy.Error.NOT_FOUND, "Gif not found")

        try:
            like = yield self.like.create(gif_id, account_id)
            self.write_json("OK")
        except IntegrityError:
            raise giffy.Error(giffy.Error.CONFLICT, "Like already exists")

    @gen.coroutine
    def delete(self, gif_id):
        gif_id = s.str2int(gif_id)
        account_id = self.account_id
        yield self.like.delete(gif_id, account_id)
        self.write_json("OK")