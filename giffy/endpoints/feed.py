import logging

import os
from tornado import gen

import giffy
from giffy.endpoints import AuthRequestHandler
from giffy import sugar as s
import magic
from psycopg2 import IntegrityError

class FeedHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, user_id=None):
        if user_id == None:
            gifs = yield self.gif.get_list()
            self.write_json(gifs)
        else:
            user_id = s.str2int(user_id)
            user = yield self.user.get(user_id)
            if user == None:
                raise giffy.Error(giffy.Error.NOT_FOUND, "User not found")
            gifs = yield self.gif.get_from_following_by_user_id(user_id)
            self.write_json(gifs)

class FeedFollowingHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self):
        gifs = yield self.gif.get_from_following_by_user_id(self.account_id)
        self.write_json(gifs)


class FeedTagHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, tag=None):
        if tag == None:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS)
        gifs = yield self.gif.get_by_tag(tag)
        self.write_json(gifs)