import logging  # noqa

from tornado import gen

import giffy
from giffy.endpoints import RequestHandler, AuthRequestHandler
from giffy import sugar as s

class UserHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, id=None):
            id = s.str2int(id)
            user = yield self.user.get(id)
            if user == None:
                raise giffy.Error(giffy.Error.NOT_FOUND, "User not found")
            self.write_json(user)

class UsersHandler(RequestHandler):
    @gen.coroutine
    def get(self):
        users = yield self.user.get_list()
        self.write_json(users)

    @gen.coroutine
    def post(self):

        username = self.get_json_argument("username", None)
        email = self.get_json_argument("email", None)
        password = self.get_json_argument("password", None)

        if username == None or email == None or password == None:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS)

        if not s.email_is_valid(email):
            raise giffy.Error(giffy.Error.BAD_PARAMETERS, "Invalid email")

        if len(username) < 3:
            raise giffy.Error(giffy.Error.BAD_PARAMETERS, "Username to short")

        if len(password) < 8:
            raise giffy.Error(giffy.Error.BAD_PARAMETERS, "Password to short")

        user = yield self.user.get_by_email(email)
        if user != None:
            raise giffy.Error(giffy.Error.CONFLICT, "Email already exists")

        user = yield self.user.get_by_username(username)
        if user != None:
            raise giffy.Error(giffy.Error.CONFLICT, "Username already exists")

        account_id = yield self.user.create(username.lower(), email.lower(), s.gen_password(password))
        token = s.random_token()
        yield self.mc.set(self.config.mc_token_key_prefix + str(token), account_id, self.config.mc_ttl)

        self.write_json({
            "accountId": account_id,
            "accessToken": token
        })

class UserLoginHandler(RequestHandler):
    @gen.coroutine
    def post(self):

        username = self.get_json_argument("username", None)
        email = self.get_json_argument("email", None)
        password = self.get_json_argument("password", None)

        if (username == None and email == None) or password == None:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS)

        if username != None:
            user = yield self.user.get_by_username(username.lower())
        else:
            user = yield self.user.get_by_email(email.lower())
        if user == None:
            raise giffy.Error(giffy.Error.UNAUTHORIZED)

        if not s.check_password(password, user.get("password")):
            raise giffy.Error(giffy.Error.UNAUTHORIZED)

        token = s.random_token()
        yield self.mc.set(self.config.mc_token_key_prefix + str(token), user.get("id"), self.config.mc_ttl)

        self.write_json({
            "accountId": user.get("id"),
            "accessToken": token
        })
