import logging

from tornado import gen
import momoko

class LikeDAO(object):
    def __init__(self, db):
        self.db = db

    @gen.coroutine
    def get(self, gif_id=0):
        sql = """
            SELECT id, username, email
            FROM users
            WHERE id IN (
                SELECT user_id
                FROM likes
                WHERE gif_id=%s)
        """
        cursor = yield self.db.execute(sql, (gif_id,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def create(self, gif_id, user_id):
        sql = """
            INSERT INTO likes (gif_id, user_id)
            VALUES (%s, %s)
            RETURNING *;
        """
        cursor = yield self.db.execute(sql, (gif_id, user_id))
        row_id = cursor.fetchone()[0]
        cursor.close()
        raise gen.Return(row_id)

    @gen.coroutine
    def delete(self, gif_id, user_id):
        sql = """
            DELETE
            FROM likes
            WHERE gif_id=%s AND user_id=%s
        """
        cursor = yield self.db.execute(sql, (gif_id,user_id))
        cursor.close()
        raise gen.Return(cursor)
