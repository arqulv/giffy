import logging

from tornado import gen
import momoko

class TagDAO(object):
    def __init__(self, db):
        self.db = db

    @gen.coroutine
    def get_by_gif_id(self, gif_id):
        sql = """
            SELECT DISTINCT tag
            FROM tags
            WHERE gif_id=%s
        """
        cursor = yield self.db.execute(sql, (gif_id,))
        desc = cursor.description
        result = {"tags": [row[0] for row in cursor.fetchall()]}
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def create(self, gif_id, user_id, tag):
        sql = """
            INSERT INTO tags (gif_id, user_id, tag)
            VALUES (%s, %s, %s)
            RETURNING *;
        """
        cursor = yield self.db.execute(sql, (gif_id, user_id, tag))
        row_id = cursor.fetchone()[0]
        cursor.close()
        raise gen.Return(row_id)