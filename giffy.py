#! /usr/bin/env python

import tornado.options
from tornado.options import define, options
from giffy import server
import random
import codecs
import sys
import os


define("config", default=None)
define("debug", default=True, type=bool)
define("env", default="dev")
define("port", default=8080)


def main():
    codecs.register_error('strict', codecs.replace_errors)
    random.seed()
    tornado.options.parse_command_line()

    sys.path.append(os.path.join(os.path.split(__file__)[0], "conf"))
    if options.config:
        exec 'import config_%s as config' % options.config
    else:
        import config

    server.run(config, options.env)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
